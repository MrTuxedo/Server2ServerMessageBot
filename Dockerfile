FROM python:bullseye

WORKDIR /usr/src/app

COPY server2serverMessageBot.py ./

RUN python3 -m pip install --upgrade pip && \
  python3 -m pip install --no-cache-dir discord.py

CMD python3 server2serverMessageBot.py