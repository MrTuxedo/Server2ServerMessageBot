# Server2ServerMessageBot

> Forked from https://github.com/birthdaysmoothie22/Server2ServerMessageBot/

## Environment

Please add env vars as in this example:
```sh
BOT_KEY=<discord_bot_key>
CHANNEL_ID_PAIRS={"sender_channel_id_1" : receiver_channel_id_1, "sender_channel_id_2" : receiver_channel_id_2}
```

## Docker Compose

It is assumed you are using a recent version of docker compose to build and run this bot:

```sh
docker compose up -d
```